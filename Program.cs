﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace EmailFinder_Regex_
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();
            SearchText(input);
        }

        static void SearchText(string EmailInInput)
        {

            Regex regex = new Regex(@"[A-Za-z0-9_\-\+]+@[A-Za-z0-9\-]+\.([A-Za-z]{2,3})(?:\.[a-z]{2})?", RegexOptions.IgnoreCase);

            MatchCollection match = regex.Matches(EmailInInput);

           

            // check if there's a match before looping to find the matches

            foreach (var m in match)
            {
                if (regex.IsMatch(m.ToString()))
                    Console.WriteLine(m);
                else
                    continue;
            }

            Console.WriteLine($"{match.Count} email(s) found");
            
            
        }
    }
}
